<?php

use App\Http\Controllers\Admin\CastController;
use App\Http\Controllers\admin\DashboardController;
use App\Http\Controllers\admin\GenreController;
use App\Http\Controllers\admin\MovieController;
use App\Http\Controllers\admin\ProfileDashboardController;
use App\Http\Controllers\superadmin\AdminController;
use App\Http\Controllers\user\AllMoviesController;
use App\Http\Controllers\user\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\user\HomeController;
use App\Http\Controllers\user\LoginController;
use App\Http\Controllers\user\RegisterController;
use Illuminate\Auth\Events\Registered;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home Controller
Route::get('/', [HomeController::class, 'go_home'])->name('home');

// All Movies Controller
Route::get('/all-movies', [AllMoviesController::class, 'index'])->name('allmovies');

//profileUser
Route::get('/profile', [ProfileController::class, 'index'])->name('profile');

// Login Controller
Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

// Register Controller
Route::get('/register', [RegisterController::class, 'index']);
Route::post('/register', [RegisterController::class, 'register']);


// Route just for super admin
Route::middleware(['auth', 'admin'])->group(function () {

    // Route to Dashboard Page
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    // Route to Profile Page
    Route::get('/dashboard/profile', [ProfileDashboardController::class, 'index'])->name('profileDashboard');

    // Route for Movie Controller
    Route::get('/dashboard/movie-controller', [MovieController::class , 'index'])->name('movie-controller');
    Route::get('/dashboard/movie-controller/tambah-movie', [MovieController::class, 'add'])->name('movie-add');

    // Route for Genre Controller
    Route::get('/dashboard/genre-controller', [GenreController::class, 'index'])->name('genre-controller');
    Route::get('/dashboard/genre-controller/tambah-genre', [GenreController::class, 'add'])->name('genre-add');
    Route::post('store-genre', [GenreController::class, 'store'])->name('genre-store');
    Route::get('/dashboard/genre-controller/edit-genre/{id}', [GenreController::class, 'edit']);
    Route::put('edit-genre/{id}', [GenreController::class, 'update'])->name('genre-update');
    Route::get('delete-genre/{id}', [GenreController::class, 'delete'])->name('genre-delete');

    // Route for Cast Controller
    Route::get('/dashboard/cast-controller', [CastController::class, 'index'])->name('cast-controller');
    Route::get('/dashboard/cast-controller/tambah-cast', [CastController::class, 'add'])->name('cast-add');
    Route::post('store-cast', [CastController::class, 'store'])->name('cast-store');
    Route::get('/dashboard/cast-controller/edit-cast/{id}', [CastController::class, 'edit']);
    Route::put('edit-cast/{id}', [CastController::class, 'update']);
    Route::get('delete-cast/{id}', [CastController::class, 'delete']);  

    // Route Just For Super Admin
    Route::middleware(['auth', 'superadmin'])->group(function() {

        // Route for Admin Controller
        Route::get('/dashboard/admin-controller', [AdminController::class, 'index'])->name('admin-controller');
        Route::get('/dashboard/admin-controller/tambah-admin', [AdminController::class, 'add'])->name('tambah-admin');
        Route::post('store-admin', [AdminController::class, 'store'])->name('admin-store');
        Route::get('/dashboard/admin-controller/edit-admin/{id}', [AdminController::class, 'edit']);
        Route::put('edit-admin/{id}', [AdminController::class, 'update']);
        Route::get('delete-admin/{id}', [AdminController::class, 'delete']);
    });

});


