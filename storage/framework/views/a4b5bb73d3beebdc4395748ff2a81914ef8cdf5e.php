<?php $__env->startSection('user'); ?>


<section class="jumbotron swiper" id="jumbotron">
    <div class="swiper-wrapper">
        <div class="swiper-slide swiper-img">
            <img src="<?php echo e(asset('assets/img/movies/home1.jpg')); ?>" alt="">
            <div class="swiper-text">
                <span>Marvel Universe</span>
                <h1>Venom Let There Be Carnage</h1>
                <a href="#" class="btn-main">See Detail</a>
            </div>
        </div>

        <div class="swiper-slide swiper-img">
            <img src="<?php echo e(asset('assets/img/movies/home2.jpg')); ?>" alt="">
            <div class="swiper-text">
                <span>Marvel Universe</span>
                <h1>Avengers Infinity Wars</h1>
                <a href="#" class="btn-main">See Detail</a>
            </div>
        </div>

        <div class="swiper-slide swiper-img">
            <img src="<?php echo e(asset('assets/img/movies/home3.jpg')); ?>" alt="">
            <div class="swiper-text">
                <span>Marvel Universe</span>
                <h1>Spider-Man Far From Home</h1>
                <a href="#" class="btn-main">See Detail</a>
            </div>
        </div>
    </div>
      
      <div class="swiper-pagination"></div>
</section>




<section class="popular">
    <h2 class="heading-popular">Top Popular</h2>
    
    
    <div class="popular-container swiper">
        <div class="swiper-wrapper">
            <div class="card-main swiper-slide">
                <a href="" class="link-movies">
                    <div class="card-img">
                        <img src="<?php echo e(asset('assets/img/movies/m8.png')); ?>" alt="">
                    </div>
                    <h3>Bumblebee</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main swiper-slide">
                <a href="" class="link-movies">
                    <div class="card-img">
                        <img src="<?php echo e(asset('assets/img/movies/home2.jpg')); ?>" alt="">
                    </div>
                    <h3>Infinity Wars</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main swiper-slide">
                <a href="" class="link-movies">
                    <div class="card-img">
                        <img src="<?php echo e(asset('assets/img/movies/home3.jpg')); ?>" alt="">
                    </div>
                    <h3>Far From Home</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main swiper-slide">
                <a href="" class="link-movies">
                    <div class="card-img">
                        <img src="<?php echo e(asset('assets/img/movies/m1.jpg')); ?>" alt="">
                    </div>
                    <h3>Venom</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main swiper-slide">
                <a href="" class="link-movies">
                    <div class="card-img">
                        <img src="<?php echo e(asset('assets/img/movies/m2.jpg')); ?>" alt="">
                    </div>
                    <h3>Dunkerk</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main swiper-slide">
                <a href="" class="link-movies">
                    <div class="card-img">
                        <img src="<?php echo e(asset('assets/img/movies/m3.jpg')); ?>" alt="">
                    </div>
                    <h3>Dunkerk</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main swiper-slide">
                <a href="" class="link-movies">
                    <div class="card-img">
                        <img src="<?php echo e(asset('assets/img/movies/m4.jpg')); ?>" alt="">
                    </div>
                    <h3>John Wick 2</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main swiper-slide">
                <a href="" class="link-movies">
                    <div class="card-img">
                        <img src="<?php echo e(asset('assets/img/movies/m5.jpg')); ?>" alt="">
                    </div>
                    <h3>Aquaman</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main swiper-slide">
                <a href="" class="link-movies">
                    <div class="card-img">
                        <img src="<?php echo e(asset('assets/img/movies/m6.jpg')); ?>" alt="">
                    </div>
                    <h3>Black Panther</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main swiper-slide">
                <a href="" class="link-movies">
                    <div class="card-img">
                        <img src="<?php echo e(asset('assets/img/movies/m7.jpg')); ?>" alt="">
                    </div>
                    <h3>Thor</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
        </div>
    </div>
</section>




<section class="new-movies" id="popular">
    <h2 class="heading-new">New Movies</h2>

    
    <div class="new-container">
        <div class="card-main">
            <a href="" class="link-movies">
                <div class="card-img new-img">
                    <img src="<?php echo e(asset('assets/img/movies/m8.png')); ?>" alt="">
                </div>
                <h3>Bumblebee</h3>
                <span>120 Min | Action</span>
            </a>
        </div>
        <div class="card-main">
            <a href="" class="link-movies">
                <div class="card-img new-img">
                    <img src="<?php echo e(asset('assets/img/movies/home2.jpg')); ?>" alt="">
                </div>
                <h3>Infinity Wars</h3>
                <span>120 Min | Action</span>
            </a>
        </div>
        <div class="card-main">
            <a href="" class="link-movies">
                <div class="card-img new-img">
                    <img src="<?php echo e(asset('assets/img/movies/home3.jpg')); ?>" alt="">
                </div>
                <h3>Far From Home</h3>
                <span>120 Min | Action</span>
            </a>
        </div>
        <div class="card-main">
            <a href="" class="link-movies">
                <div class="card-img new-img">
                    <img src="<?php echo e(asset('assets/img/movies/m1.jpg')); ?>" alt="">
                </div>
                <h3>Venom</h3>
                <span>120 Min | Action</span>
            </a>
        </div>
        <div class="card-main">
            <a href="" class="link-movies">
                <div class="card-img new-img">
                    <img src="<?php echo e(asset('assets/img/movies/m2.jpg')); ?>" alt="">
                </div>
                <h3>Dunkerk</h3>
                <span>120 Min | Action</span>
            </a>
        </div>
        <div class="card-main">
            <a href="" class="link-movies">
                <div class="card-img new-img">
                    <img src="<?php echo e(asset('assets/img/movies/m3.jpg')); ?>" alt="">
                </div>
                <h3>Batman VS Superman</h3>
                <span>120 Min | Action</span>
            </a>
        </div>
        <div class="card-main">
            <a href="" class="link-movies">
                <div class="card-img new-img">
                    <img src="<?php echo e(asset('assets/img/movies/m4.jpg')); ?>" alt="">
                </div>
                <h3>John Wick 2</h3>
                <span>120 Min | Action</span>
            </a>
        </div>
        <div class="card-main">
            <a href="" class="link-movies">
                <div class="card-img new-img">
                    <img src="<?php echo e(asset('assets/img/movies/m5.jpg')); ?>" alt="">
                </div>
                <h3>Aquaman</h3>
                <span>120 Min | Action</span>
            </a>
        </div>
        <div class="card-main">
            <a href="" class="link-movies">
                <div class="card-img new-img">
                    <img src="<?php echo e(asset('assets/img/movies/m6.jpg')); ?>" alt="">
                </div>
                <h3>Black Panther</h3>
                <span>120 Min | Action</span>
            </a>
        </div>
        <div class="card-main">
            <a href="" class="link-movies">
                <div class="card-img new-img">
                    <img src="<?php echo e(asset('assets/img/movies/m7.jpg')); ?>" alt="">
                </div>
                <h3>Thor</h3>
                <span>120 Min | Action</span>
            </a>
        </div>
    </div>
</section>



<?php $__env->stopSection(); ?>


<?php echo $__env->make('main.user.usermain', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Tugas & Project\Project\Laravel-Project\movieque-app\resources\views/index.blade.php ENDPATH**/ ?>