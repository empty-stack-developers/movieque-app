<!doctype html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    

        
        <link rel="stylesheet" href="<?php echo e(asset('assets/css/style.css')); ?>">

         
        <link rel="stylesheet"href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css"/>

        
        <link rel="stylesheet" href="<?php echo e(asset('assets/css/bootstrap.min.css')); ?>">

        
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        
      
    

    
    <title><?php echo e($title); ?> | Movieque</title>
    <link rel="icon" type="image/x-icon" href="<?php echo e(asset('assets/img/logo/logo.png')); ?>">
    
</head>
<body>


     <?php echo $__env->make('main.parts.user.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->yieldContent('user'); ?>
     <?php echo $__env->make('main.parts.user.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>




    

        
        <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
        
        
        <script src="<?php echo e(asset('assets/js/script.js')); ?>"></script>

        
        <script src="<?php echo e(asset('assets/js/bootstrap.min.js')); ?>"></script>

    

</body> 
</html>
<?php /**PATH D:\Tugas & Project\Project\Laravel-Project\movieque-app\resources\views/main/user/usermain.blade.php ENDPATH**/ ?>