<section class="footer">
    <a href="" class="logo">
        <i class='bx bxs-movie'></i> Movieque
    </a>
    <div class="social">
        <a href="#"><i class='bx bxl-facebook' ></i></a>
        <a href="#"><i class='bx bxl-instagram' ></i></a>
        <a href="#"><i class='bx bxl-github' ></i></a>
        <a href="#"><i class='bx bxl-twitter' ></i></a>
    </div>
</section>
<div class="copyright">
    <p>&#169; Movieque All Right Reserved.</p>
</div><?php /**PATH D:\Tugas & Project\Project\Laravel-Project\movieque-app\resources\views/main/parts/user/footer.blade.php ENDPATH**/ ?>