
<header>
    <a href="<?php echo e(route('home')); ?>" class="logo">
        <i class='bx bxs-movie'></i> Movieque
    </a>

    <div class="bx bx-menu" id="menu-icon"></div>

    <ul class="navbar-main mb-0">
        <li><a href="<?php echo e(route('home')); ?>" class="<?php echo e(Request::is('/') ? 'home-active':''); ?>">Home</a></li>
        <li><a href="<?php echo e(route('allmovies')); ?>" class="<?php echo e(Request::is('all-movies') ? 'home-active':''); ?>">Movies</a></li>
        <li><a href="#genres">Genres</a></li>
        <li><a href="#about">About</a></li>
    </ul>
    <?php if(auth()->guard()->check()): ?>
        <img class="img-drop" id="drop-pop" onclick="popup()" src="<?php echo e(asset('assets/img/icon/user-circle.png')); ?>" alt="" width="35" height="35">
            <div class="dropdown-main" id="dropdown-main">
                <span class="name-drop"><?php echo e(Auth()->user()->name); ?></span>
                <?php if(Auth()->user()->role_as == '1' || auth()->user()->role_as == '2'): ?>
                    <a class="drowdown-link" href="<?php echo e(url('/dashboard')); ?>"><i class='bx bxs-dashboard' ></i> <span class="span-nav">Dashboard</span></a>
                <?php endif; ?>
                <a class="drowdown-link" href=""><i class='bx bxs-user'></i> <span class="span-nav">Profile</span></a>
                <button type="button" class="drowdown-link btn-logout" data-bs-toggle="modal" data-bs-target="#logoutModal"><i class='bx bxs-log-out' ></i> <span class="span-nav">Logout</span></button>
            </div>
     <?php else: ?>
        <a href="<?php echo e(url('/login')); ?>" class="btn-main">Sign In </a>
    <?php endif; ?>
</header>
  
  <!-- Modal -->
  <div class="modal fade" id="logoutModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content bg-dark">
        <form action="<?php echo e(url('/logout')); ?>" method="POST">
            <?php echo csrf_field(); ?>
            <div class="modal-body text-white text-center mt-3">
                Yakin Untuk Logout ?
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-danger btn-sm">Log Out</button>
            </div>
        </form>
      </div>
    </div>
  </div>

<?php /**PATH D:\Tugas & Project\Project\Laravel-Project\movieque-app\resources\views/main/parts/user/navbar.blade.php ENDPATH**/ ?>