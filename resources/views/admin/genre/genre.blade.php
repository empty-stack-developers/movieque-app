@extends('main.admin.adminmain')

@section('admin')
    
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="d-flex justify-content-between align-items-center mb-4">
                                <h3 class="box-title">{{ $title }} Table</h3>
                                <a href="{{ route('genre-add') }}" class="btn btn-outline-primary fw-bold"><span class=""><i class='bx bx-folder-plus'></i> Tambah Genre</span></a>
                            </div>
                            <div class="table-responsive">
                                <table class="table text-nowrap" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th class="border-top-0">No</th>
                                            <th class="border-top-0">Nama Genre</th>
                                            <th class="border-top-0">Deskripsi</th>
                                            <th class="border-top-0">Status</th>
                                            <th class="border-top-0">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($genre as $data)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $data->name }}</td>
                                                <td>{{ $data->description }}</td>
                                                <td>
                                                    @if($data->status == '1')
                                                        <span class="text-primary fw-bold fs-3">Aktif</span>
                                                        @elseif($data->status == '0')
                                                            <span class="text-danger fw-bold fs-3">Tidak Aktif</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ url('/dashboard/genre-controller/edit-genre/'. Crypt::encryptString($data->id)) }}" class="bg-transparent border-0 button-aksi text-warning px-2"><i class='bx bx-edit' style="font-size: 1.2rem"></i></a>
                                                    <!-- Button trigger modal -->
                                                    <button class="bg-transparent border-0 button-aksi text-danger px-2" data-bs-toggle="modal" data-bs-target="#deleteGenre{{ $data->id }}"><i class='bx bx-trash' style="font-size: 1.2rem"></i></button>
                                                    
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="deleteGenre{{ $data->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                            <h1 class="modal-title fs-5" id="exampleModalLabel">Hapus Genre</h1>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="">
                                                                    <div>
                                                                        <h4>Data yang akan di hapus :</h4>
                                                                    </div>
                                                                    <div class="container">
                                                                        <div class="row py-1" style="border-left: 1px solid #ff2c1f">
                                                                            <div class="col col-lg-5">
                                                                                Nama Genre
                                                                            </div> 
                                                                            <div class="col">
                                                                               <span class="fw-bold">:</span> {{ $data->name }}
                                                                            </div>
                                                                        </div>
                                                                        <div class="row py-1" style="border-left: 1px solid #ff2c1f">
                                                                            <div class="col col-lg-5">
                                                                                Deskripsi
                                                                            </div> 
                                                                            <div class="col">
                                                                               <span class="fw-bold">:</span> {{ $data->description }}
                                                                            </div>
                                                                        </div>
                                                                        <div class="row py-1" style="border-left: 1px solid #ff2c1f">
                                                                            <div class="col col-lg-5">
                                                                                Status 
                                                                            </div> 
                                                                            <div class="col">
                                                                               <span class="fw-bold">:</span> @if ($data->status == '1')
                                                                                    <span class="text-primary fw-bold">Aktif</span>
                                                                                    @elseif ($data->status == '0')
                                                                                        <span class="text-danger fw-bold">Tidak Aktif</span>
                                                                               @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal">Batal</button>
                                                            <a href="{{ url('delete-genre/'.Crypt::encryptString($data->id)) }}" class="btn btn-delete text-white">Hapus Genre</a>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@endsection