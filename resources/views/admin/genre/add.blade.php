@extends('main.admin.adminmain')

@section('admin')
    
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="d-flex justify-content-between align-items-center">
                                <h3 class="box-title">{{ $title }}</h3>
                            </div>
                            <form class="mt-3 container" action="{{ route('genre-store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                                <div class="mb-3 d-flex justify-content-between">
                                    <div class="w-100 me-3">
                                        <label class="form-label">Nama Genre</label>
                                        <input name="name" type="text" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Nama genre...">
                                        @error('name')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="w-100">
                                        <label class="form-label">Slug Genre</label>
                                        <input name="slug" type="text" id="slug" class="form-control @error('slug') is-invalid @enderror" value="{{ old('slug') }}" placeholder="Slug genre...">
                                        @error('slug')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Deskripsi Genre</label>
                                    <textarea name="description" class="form-control @error('description') is-invalid @enderror" rows="3" placeholder="Tuliskan deskripsi genre....">{{ old('description') }}</textarea>
                                    @error('description')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                {{-- <div class="form-check">
                                    <input name="status" class="form-check-input" type="checkbox">
                                    <label class="form-check-label fw-bold">
                                    Active?
                                    </label>
                                </div> --}}
                                <div class="form-check form-switch">
                                    <input name="status" class="form-check-input" type="checkbox" role="switch">
                                    <label class="form-check-label">Active ?</label>
                                </div>
                                  
                                <div class="mt-2 text-end">
                                    <button type="submit" class="btn text-white" style="background: #ff2c1f;"><i class='bx bx-folder-plus'></i> Tambah Genre</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@endsection