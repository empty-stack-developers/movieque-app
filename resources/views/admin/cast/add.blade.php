@extends('main.admin.adminmain')

@section('admin')
             <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-12">
                        <div class="white-box">
                            <div class="user-bg"> 
                                <div class="overlay-box">
                                    <img class="img-preview" >
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <!-- Column -->

                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form-horizontal form-material" action="{{ route('cast-store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                    <div class="mb-4 d-flex justify-content-between">
                                        <div class="w-100 me-3">
                                            <label class="form-label col-md-12">Nama Pemain</label>
                                            <input name="name" type="text" id="name" class="col-md-12 form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Nama pemain...">
                                            @error('name')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="w-100">
                                            <label class="form-label col-md-12">Slug</label>
                                            <input name="slug" type="text" id="slug" class="col-md-12 form-control @error('slug') is-invalid @enderror" value="{{ old('slug') }}" placeholder="Slug pemain...">
                                            @error('slug')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-4 d-flex justify-content-between">
                                        <div class="w-100 me-3">
                                            <label class="form-label col-md-12">Tanggal Lahir</label>
                                            <input name="tgl_lahir" type="date" id="tgl_lahir" class="col-md-12 form-control @error('tgl_lahir') is-invalid @enderror" value="{{ old('tgl_lahir') }}" placeholder="Tanggal Lahir...">
                                            @error('tgl_lahir')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="w-100">
                                            <label class="form-label col-md-12">Tempat Lahir</label>
                                            <input name="tmpt_lahir" type="text" id="tmpt_lahir" class="col-md-12 form-control @error('tmpt_lahir') is-invalid @enderror" value="{{ old('tmpt_lahir') }}" placeholder="Tempat Lahir...">
                                            @error('tmpt_lahir')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-4 d-flex justify-content-between">
                                        <div class="w-100 me-3">
                                            <label class="form-label col-md-12">Umur</label>
                                            <input name="umur" type="text" id="umur" class="col-md-12 form-control @error('umur') is-invalid @enderror" value="{{ old('umur') }}" placeholder="Umur...">
                                            @error('umur')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="w-100">
                                            <label class="form-label col-md-12">Negara Asal</label>
                                            <input name="negara_asal" type="text" id="negara_asal" class="col-md-12 form-control @error('negara_asal') is-invalid @enderror" value="{{ old('negara_asal') }}" placeholder="Negara Asal...">
                                            @error('negara_asal')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-4">
                                        <label class="form-label col-md-12">Foto Pemain</label>
                                        <input name="img_cast" type="file" accept="image/*" id="image" class="col-md-12 image form-control @error('img_cast') is-invalid @enderror" value="{{ old('img_cast') }}" placeholder="Nama pemain..." onchange="previewImage()">
                                        @error('img_cast')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="mb-4">
                                        <label class="form-label col-md-12">Deskripsi Pemain</label>
                                        <input id="description" type="hidden" name="description" class="col-md-12">
                                        <trix-editor input="description" class="@error('description') is-invalid @enderror" style="@error('description') border: 1px solid red @enderror"></trix-editor>
                                        @error('description')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    
                                    <div class="form-check form-switch">
                                        <input name="status" class="form-check-input col-md-12" type="checkbox" role="switch">
                                        <label class="form-check-label col-md-12">Active ?</label>
                                    </div>
                                      
                                    <div class="mt-2 text-end col-md-12">
                                        <button type="submit" class="btn text-white" style="background: #ff2c1f;"><i class='bx bx-folder-plus'></i> {{ $title }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
@endsection

