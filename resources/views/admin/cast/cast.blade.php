@extends('main.admin.adminmain')

@section('admin')
    
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="d-flex justify-content-between align-items-center mb-4">
                                <h3 class="box-title">{{ $title }} Table</h3>
                                <a href="{{ route('cast-add') }}" class="btn btn-outline-primary fw-bold"><span class=""><i class='bx bx-folder-plus'></i> Tambah {{ $subtitle }}</span></a>
                            </div>
                            <div class="table-responsive">
                                <table class="table text-nowrap" id="dataTable"> 
                                    <thead>
                                        <tr>
                                            <th class="border-top-0">No</th>
                                            <th class="border-top-0">Nama Pemain</th>
                                            <th class="border-top-0">Tanggal Lahir</th>
                                            <th class="border-top-0">Asal Pemain</th>
                                            <th class="border-top-0">Status</th>
                                            <th class="border-top-0">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($cast as $data)
                                        <tr>
                                            <td style="vertical-align:middle">{{ $loop->iteration }}</td>

                                            <td>
                                                @if($data->img_cast)
                                                <button type="button" class="border-0 bg-white btn-cast" data-bs-toggle="modal" data-bs-target="#castPhoto{{ $data->id }}">
                                                    <img class="rounded-pill" src="{{ asset('assets/uploads/cast/'.$data->img_cast) }}" alt="{{ $data->name }}" width="40" height="40" title="{{ $data->name }}"> 
                                                </button>
                                                    @else
                                                    <button type="button" class="border-0 bg-white btn-cast" data-bs-toggle="modal" data-bs-target="#castPhoto{{ $data->id }}">
                                                        <img class="rounded-pill" src="{{ asset('assets/img/user/d1.jpg') }}" alt="{{ $data->name }}" width="40" height="40" title="{{ $data->name }}"> 
                                                    </button>
                                                @endif
                                                <span class="ms-2">{{ $data->name }}</span>
                                                <!-- Modal -->
                                                <div class="modal fade" id="castPhoto{{ $data->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                        <h1 class="modal-title fs-5" id="exampleModalLabel">{{ $data->name }} Photo</h1>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                        @if($data->img_cast)
                                                            <div class="text-center">
                                                                <img src="{{ asset('assets/uploads/cast/'.$data->img_cast) }}" class="img-fluid">
                                                            </div>
                                                            @else
                                                                <div class="text-center">
                                                                    <img src="{{ asset('assets/img/user/d1.jpg') }}" class="img-fluid">
                                                                </div>
                                                        @endif
                                                        </div>
                                                        <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </td>

                                            <td style="vertical-align:middle">{{ $data->tgl_lahir->format('d F Y') }}</td>

                                            <td style="vertical-align:middle">{{ $data->negara_asal }}</td>
                                            
                                            <td style="vertical-align:middle">
                                                @if ($data->status == '1')
                                                    <span class="text-primary fw-bold fs-3">Aktif</span>
                                                    @elseif ($data->status == '0')
                                                        <span class="text-danger fw-bold fs-3">Tidak Aktif</span>
                                                @endif
                                            </td>
                                            
                                            <td style="vertical-align:middle">
                                                <a href="{{ url('/dashboard/cast-controller/edit-cast/'.Crypt::encryptString($data->id)) }}" class="bg-transparent border-0 button-aksi text-warning px-2"><i class='bx bx-edit' style="font-size: 1.2rem"></i></a>
                                                
                                                <!-- Button trigger modal -->
                                                <button class="bg-transparent border-0 button-aksi text-danger px-2" data-bs-toggle="modal" data-bs-target="#deleteCast{{ $data->id }}"><i class='bx bx-trash' style="font-size: 1.2rem"></i></button>
                                                
                                                <!-- Modal -->
                                                <div class="modal fade" id="deleteCast{{ $data->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                        <h1 class="modal-title fs-5" id="exampleModalLabel">Hapus {{ $subtitle }}</h1>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="">
                                                                <div>
                                                                    <h4>Data yang akan di hapus :</h4>
                                                                </div>
                                                                <div class="container">
                                                                    <div class="row py-1" style="border-left: 1px solid #ff2c1f">
                                                                        <div class="col col-lg-5">
                                                                            Nama Pemain
                                                                        </div> 
                                                                        <div class="col">
                                                                           <span class="fw-bold">:</span> {{ $data->name }}
                                                                        </div>
                                                                    </div>
                                                                    <div class="row py-1" style="border-left: 1px solid #ff2c1f">
                                                                        <div class="col col-lg-5">
                                                                            Tanggal Lahir
                                                                        </div> 
                                                                        <div class="col">
                                                                           <span class="fw-bold">:</span> {{ $data->tgl_lahir->format('d F Y') }}
                                                                        </div>
                                                                    </div>
                                                                    <div class="row py-1" style="border-left: 1px solid #ff2c1f">
                                                                        <div class="col col-lg-5">
                                                                            Tempat Lahir
                                                                        </div> 
                                                                        <div class="col">
                                                                           <span class="fw-bold">:</span> {{ $data->tmpt_lahir }}
                                                                        </div>
                                                                    </div>
                                                                    <div class="row py-1" style="border-left: 1px solid #ff2c1f">
                                                                        <div class="col col-lg-5">
                                                                            Asal Pemain 
                                                                        </div> 
                                                                        <div class="col">
                                                                           <span class="fw-bold">:</span> {{ $data->negara_asal }}
                                                                        </div>
                                                                    </div>
                                                                    <div class="row py-1" style="border-left: 1px solid #ff2c1f">
                                                                        <div class="col col-lg-5">
                                                                            Umur Pemain 
                                                                        </div> 
                                                                        <div class="col">
                                                                           <span class="fw-bold">:</span> {{ $data->umur }} Tahun
                                                                        </div>
                                                                    </div>
                                                                    <div class="row py-1" style="border-left: 1px solid #ff2c1f">
                                                                        <div class="col col-lg-5">
                                                                            Status 
                                                                        </div> 
                                                                        <div class="col">
                                                                           <span class="fw-bold">:</span> @if ($data->status == '1')
                                                                                <span class="text-primary fw-bold">Aktif</span>
                                                                                @elseif ($data->status == '0')
                                                                                    <span class="text-danger fw-bold">Tidak Aktif</span>
                                                                           @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal">Batal</button>
                                                        <a href="{{ url('delete-cast/'.Crypt::encryptString($data->id)) }}" class="btn btn-delete text-white">Hapus {{ $subtitle }}</a>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@endsection