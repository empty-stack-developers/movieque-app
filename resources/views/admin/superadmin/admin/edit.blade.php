@extends('main.admin.adminmain')

@section('admin')
    
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="d-flex justify-content-between align-items-center">
                                <h3 class="box-title">{{ $title }}</h3>
                            </div>
                            <form class="mt-3 container" action="{{ url('edit-admin/'.$admin->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                                <div class="mb-3 d-flex justify-content-between">
                                    <div class="w-100 me-3">
                                        <label class="form-label">Nama Admin</label>
                                        <input name="name" type="text" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', $admin->name) }}" placeholder="Nama admin...">
                                        @error('name')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="w-100">
                                        <label class="form-label">Username Admin</label>
                                        <input name="username" type="text" id="username" class="form-control @error('username') is-invalid @enderror" value="{{ old('username', $admin->username) }}" placeholder="Username admin...">
                                        @error('username')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="w-100 mb-3">
                                    <label class="form-label">Email Admin</label>
                                    <input name="email" type="email" id="username" class="form-control @error('email') is-invalid @enderror" value="{{ old('email', $admin->email) }}" placeholder="Email admin...">
                                    @error('email')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="mb-3 d-flex justify-content-between">
                                    <div class="w-100 me-3">
                                        <label class="form-label">Password</label>
                                        <input name="password" type="password" id="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password admin...">
                                        @error('password')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="w-100">
                                        <label class="form-label">Re Password</label>
                                        <input name="re_password" type="password" id="re-pass" class="form-control @error('re_password') is-invalid @enderror" placeholder="Ulangi Password...">
                                        @error('re_password')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="mt-2 text-end">
                                    <button type="submit" class="btn text-white" style="background: #ff2c1f;"><i class='bx bx-edit'></i> Update Admin</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@endsection