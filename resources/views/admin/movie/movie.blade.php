@extends('main.admin.adminmain')

@section('admin')
    
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="d-flex justify-content-between align-items-center">
                                <h3 class="box-title">{{ $title }} Table</h3>
                                <a href="{{ route('movie-add') }}" class="btn btn-outline-primary fw-bold"><span class=""><i class='bx bx-folder-plus'></i> Tambah Genre</span></a>
                            </div>
                            <div class="table-responsive">
                                <table class="table text-nowrap">
                                    <thead>
                                        <tr>
                                            <th class="border-top-0">No</th>
                                            <th class="border-top-0">Nama Movie</th>
                                            <th class="border-top-0">Nama Produser</th>
                                            <th class="border-top-0">Status</th>
                                            <th class="border-top-0">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Venom: Let There Be Carnage</td>
                                            <td>Prohaska</td>
                                            <td>
                                                <span class="text-primary fw-bold fs-3">Aktif</span>
                                            </td>
                                            <td>
                                                <a href="" class="bg-transparent border-0 button-aksi text-warning px-2"><i class='bx bx-edit' style="font-size: 1.2rem"></i></a>
                                                <!-- Button trigger modal -->
                                                <button class="bg-transparent border-0 button-aksi text-danger px-2" data-bs-toggle="modal" data-bs-target="#deleteMovie"><i class='bx bx-trash' style="font-size: 1.2rem"></i></button>
                                                
                                                <!-- Modal -->
                                                <div class="modal fade" id="deleteMovie" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                        <h1 class="modal-title fs-5" id="exampleModalLabel">Hapus Genre</h1>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                        Yakin Untuk Menghapus Data Movie Ini ?
                                                        </div>
                                                        <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal">Batal</button>
                                                        <a href="" class="btn btn-delete text-white">Hapus Genre</a>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

@endsection