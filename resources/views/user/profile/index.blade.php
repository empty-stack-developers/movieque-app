@extends('main.user.usermain')

@section('user')
    
    <section class="mt-5">
        <h2 class="heading-allmovies">Profile Accounts</h2>

        <div class="allmovies-container">
            <form action="{{ route('updateProfile') }}" method="POST">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-item">
                            <div class="card-body py-2 px-4 rounded-2" style="background-color: #0f0f0f;" width="50%">
                                <div class="text-center my-3">
                                    <img src="{{ asset('assets/img/user/d1.jpg') }}" class="img-preview rounded-circle overflow-hidden border border-dark" alt="" width="100px" height="100px">  
                                    <input type="file" id="image" class="custom-file-input my-3 " name="user_img" onchange="previewImage()">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card-item ">
                            <div class="card-body rounded-2" style="background-color: #0f0f0f;">
                                <div class="form-group mx-4">
                                    <label class="my-2" for="name">Name</label>
                                    <input type="text" id="name" class="form-control" name="name" value="{{ Auth()->user()->name }}">
                                </div>
                                <div class="form-group mx-4">
                                    <label class="my-2" for="username">Username</label>
                                    <input type="text" id="username" class="form-control" name="username" value="{{ Auth()->user()->username }}">
                                </div>
                                <div class="form-group mx-4">
                                    <label class="my-2" for="email">Email</label>
                                    <input type="email" id="email" class="form-control" name="email" value="{{ Auth()->user()->email }}">
                                </div>
                                <div class="form-group mx-4">
                                    <label class="my-2" for="resetpass">Reset password</label>
                                    <input type="password" id="resetpass" class="form-control" name="password" value="">
                                </div> 
                                <div class="w-1 d-flex my-4 mx-3 pb-4 flex-row">
                                    <a href="{{ route('home') }}" class="btn btn-danger mx-2">Back</a>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
<script>
    function previewImage() {
        const image = document.querySelector('#image');
        const imgPreview = document.querySelector('.img-preview');

        // imgPreview.style.display = 'block';

        const oFReader = new FileReader();
        oFReader.readAsDataURL(image.files[0]);

        oFReader.onload = function(oFREvent) {
            imgPreview.src = oFREvent.target.result;
        }
    }

</script>


@endsection