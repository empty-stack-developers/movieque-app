@extends('main.user.usermain')

@section('user')
    
    <section class="mt-5">
        <h2 class="heading-allmovies">All Movies</h2>
        
        <div class="search-movies py-3 px-4 d-flex justify-content-between">
            <div>
                <form action="">
                    <input type="search" name="search-movies" class="input-search px-2 py-1" placeholder="Search movies...">
                    <button type="submit" class="btn-search px-2 py-1 ms-1"><i class='bx bx-search'></i></button>
                </form>
            </div>
            <div>
                <select class="genre-search px-2 py-1">
                    <option selected>Select Genre</option>
                    @foreach ($genre as $i)
                        <option value="{{ $i->id }}">{{ $i->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="allmovies-container">
            <div class="card-main">
                <a href="" class="link-movies">
                    <div class="card-img new-img">
                        <img src="{{ asset('assets/img/movies/m8.png') }}" alt="">
                    </div>
                    <h3>Bumblebee</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main">
                <a href="" class="link-movies">
                    <div class="card-img new-img">
                        <img src="{{ asset('assets/img/movies/home2.jpg') }}" alt="">
                    </div>
                    <h3>Infinity Wars</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main">
                <a href="" class="link-movies">
                    <div class="card-img new-img">
                        <img src="{{ asset('assets/img/movies/home3.jpg') }}" alt="">
                    </div>
                    <h3>Far From Home</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main">
                <a href="" class="link-movies">
                    <div class="card-img new-img">
                        <img src="{{ asset('assets/img/movies/m1.jpg') }}" alt="">
                    </div>
                    <h3>Venom</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main">
                <a href="" class="link-movies">
                    <div class="card-img new-img">
                        <img src="{{ asset('assets/img/movies/m2.jpg') }}" alt="">
                    </div>
                    <h3>Dunkerk</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main">
                <a href="" class="link-movies">
                    <div class="card-img new-img">
                        <img src="{{ asset('assets/img/movies/m3.jpg') }}" alt="">
                    </div>
                    <h3>Batman VS Superman</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main">
                <a href="" class="link-movies">
                    <div class="card-img new-img">
                        <img src="{{ asset('assets/img/movies/m4.jpg') }}" alt="">
                    </div>
                    <h3>John Wick 2</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main">
                <a href="" class="link-movies">
                    <div class="card-img new-img">
                        <img src="{{ asset('assets/img/movies/m5.jpg') }}" alt="">
                    </div>
                    <h3>Aquaman</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main">
                <a href="" class="link-movies">
                    <div class="card-img new-img">
                        <img src="{{ asset('assets/img/movies/m6.jpg') }}" alt="">
                    </div>
                    <h3>Black Panther</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
            <div class="card-main">
                <a href="" class="link-movies">
                    <div class="card-img new-img">
                        <img src="{{ asset('assets/img/movies/m7.jpg') }}" alt="">
                    </div>
                    <h3>Thor</h3>
                    <span>120 Min | Action</span>
                </a>
            </div>
        </div>
    </section>

@endsection