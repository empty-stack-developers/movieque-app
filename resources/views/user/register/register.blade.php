<!doctype html>
<html lang="id">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.101.0">

    {{-- Title --}}
    <title>{{ $title }} | Movieque</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/logo/logo.png') }}">

    {{-- Bootstrap CSS --}}
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .b-example-divider {
        height: 3rem;
        background-color: rgba(0, 0, 0, .1);
        border: solid rgba(0, 0, 0, .15);
        border-width: 1px 0;
        box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
      }

      .b-example-vr {
        flex-shrink: 0;
        width: 1.5rem;
        height: 100vh;
      }

      .bi {
        vertical-align: -.125em;
        fill: currentColor;
      }

      .nav-scroller {
        position: relative;
        z-index: 2;
        height: 2.75rem;
        overflow-y: hidden;
      }

      .nav-scroller .nav {
        display: flex;
        flex-wrap: nowrap;
        padding-bottom: 1rem;
        margin-top: -1px;
        overflow-x: auto;
        text-align: center;
        white-space: nowrap;
        -webkit-overflow-scrolling: touch;
      }
    </style>

    
    <!-- My CSS -->
    <link href="{{ asset('assets/css/loginregister.css') }}" rel="stylesheet">

    {{-- Boxicon --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">

  </head>
  <body class="text-center">
    
    <main class="form-signin w-100 m-auto">
      <form action="{{ url('/register') }}" method="POST">
        @csrf
        <img class="mb-1" src="{{ asset('assets/img/logo/logo.png') }}" alt="" width="72" height="57">
        <h1 class="h3 mb-5">Register Movieque</h1>

        <div class="mb-2">
          <div class="d-flex position-relative">
            <i class='bx bx-user position-absolute icon-form' style='color:#ff2c1f'></i>
            <input type="text" name="name" class="form-control input-form" placeholder="Your Name..." value="{{ old('name') }}">
          </div>
          @error('name')
            <div class="alert-form-body">
              <span class="text-muted ms-1 alert-form mb-2" style="color: #ff2c1f !important;">{{ $message }}</span>
            </div>
          @enderror
        </div>
        <div class="mb-2">
          <div class="d-flex position-relative">
            <i class='bx bx-user position-absolute icon-form' style='color:#ff2c1f'></i>
            <input type="text" name="username" class="form-control input-form" placeholder="Your Username..." value="{{ old('username') }}">
          </div>
          @error('username')
            <div class="alert-form-body">
              <span class="text-muted ms-1 alert-form mb-2" style="color: #ff2c1f !important;">{{ $message }}</span>
            </div>
          @enderror
        </div>
        <div class="mb-2">
          <div class="d-flex position-relative">
            <i class='bx bxs-envelope position-absolute icon-form' style='color:#ff2c1f'></i>
            <input type="email" name="email" class="form-control input-form" placeholder="Email..." value="{{ old('email') }}">
          </div>
          @error('email')
            <div class="alert-form-body">
              <span class="text-muted ms-1 alert-form mb-2" style="color: #ff2c1f !important;">{{ $message }}</span>
            </div>
          @enderror
        </div>
        <div class="mb-1">
          <div class="d-flex position-relative">
            <i class='bx bx-lock-alt position-absolute icon-form lock' style='color:#ff2c1f'  ></i>
            <input type="password" name="password" class="form-control input-form" placeholder="Password...">
          </div>
          <div class="span-muted">
              <span class="text-muted ms-1"><i class='bx bxs-lock-alt' style='color:#ff2c1f;'></i> Password must be at least 8 characters</span>
          </div>
        </div>
        <div class="mb-1">
          <div class="d-flex position-relative">
            <i class='bx bx-repeat position-absolute icon-form' style='color:#ff2c1f'  ></i>
            <input type="password" name="re_password" class="form-control input-form mt-2" placeholder="Re-Enter Password...">
          </div>
          @error('password')
            <div class="alert-form-body">
              <span class="text-muted ms-1 alert-form" style="color: #ff2c1f !important;">{{ $message }}</span>
            </div>
          @enderror
        </div>

        <button class="w-100 btn btn-lg mt-4" type="submit">Register</button>
        <span class="text-muted small">Sudah Memiliki Akun? <a href="{{ url('/login') }}" class="regis-link">Login!</a></span>
        <p class="mt-3 mb-3 text-muted">&copy; Movique 2017–2022</p>
      </form>
    </main>

    {{-- Bootstrap JS --}}
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    
  </body>
</html>


