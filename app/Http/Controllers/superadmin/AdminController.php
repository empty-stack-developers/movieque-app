<?php

namespace App\Http\Controllers\superadmin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class AdminController extends Controller
{
    // Route to admin controller page
    public function index() {
        $admin = User::where('role_as', '1')->get();

        return view('admin.superadmin.admin.admin', [
            'title' => 'Admin Data',
            'subtitle' => 'Admin'
        ], compact('admin'));
    }

    // Route to add admin page
    public function add() {
        return view('admin.superadmin.admin.add', [
            'title' => 'Tambah Admin',
        ]);
    }

    // Add Admin Function
    public function store(Request $request) {
        $admin = new User();

        $validateData = $request->validate([
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:8|same:re_password',
        ]);


        // Hash Password
        $hash = Hash::make($validateData['password']);
       
        $admin->name = $request->input('name');
        $admin->username = $request->input('username');
        $admin->email = $request->input('email');
        $admin->role_as = '1';
        $admin->password = $hash;


        $admin->save($validateData);
        return redirect('/dashboard/admin-controller')->with('success', 'Admin Berhasil Dibuat!');
    }

    // Route to edit admin page
    public function edit($id) {

        // Mendecrypt id 
        $decryptedID = Crypt::decryptString($id);
        $admin = User::find($decryptedID);

        return view ('admin.superadmin.admin.edit', [
            'title' => 'Edit Admin'
        ], compact('admin'));
    }

    // Edit Admin Function
    public function update(Request $request, $id) {
        $admin = User::find($id);

        // Validasi Data
        $validateData = $request->validate([
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email:dns',
            'password' => 'required|min:8|same:re_password',
        ]);

        // Hash Password
        $hash = Hash::make($validateData['password']);
        
        $admin->name = $request->input('name');
        $admin->username = $request->input('username');
        $admin->email = $request->input('email');
        $admin->role_as = '1';
        $admin->password = $hash;

        $admin->update();
        return redirect('/dashboard/admin-controller')->with('success', 'Admin Berhasil di Edit!');
    }

    public function delete($id) {

        // Mendecrypt id 
        $decryptedID = Crypt::decryptString($id);
        $admin = User::find($decryptedID);

        $admin->delete();
        return back()->with('success', 'Admin Berhasil di Hapus!');


    }
}
