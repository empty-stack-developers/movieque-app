<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    // Routes to dashboard page
    public function index() {
        return view('admin.dashboard.dashboard', [
            'title' => 'Dashboard'
        ]);
    }
}
