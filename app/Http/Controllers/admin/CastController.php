<?php

namespace App\Http\Controllers\Admin;

use App\Models\Cast;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;

class CastController extends Controller
{
    // Route To Cast Data Page
    public function index() {
        $cast = Cast::latest()->get();

        return view('admin.cast.cast', [
            'title' => 'Cast Data',
            'subtitle' => 'Cast'
        ], compact('cast'));
    }

    // Route to Cast Add Page
    public function add() {
        return view('admin.cast.add', [
            'title' => 'Tambah Cast'
        ]);
    }

    // Add Data Cast Function
    public function store(Request $request) {
        $cast = new Cast();

        // Validate Data Cast
        $validateData = $request->validate([
            'name' => 'required|unique:cast',
            'slug' => 'required',
            'tgl_lahir' => 'required',
            'tmpt_lahir' => 'required',
            'umur' => 'required',
            'negara_asal' => 'required',
            'img_cast' => 'image|nullable',
            'description' => 'required'
        ]);

        // Upload Photo Cast 
        if($request->hasFile('img_cast')) {
            $file = $request->file('img_cast');
            $ext = $file->getClientOriginalExtension();
            $filename = time().'.'.$ext;
            $file->move('assets/uploads/cast',$filename);
            $cast->img_cast = $filename;
        }

        // Upload the Data
        $cast->name = $request->input('name');
        $cast->slug = $request->input('slug');
        $cast->tgl_lahir = $request->input('tgl_lahir');
        $cast->tmpt_lahir = $request->input('tmpt_lahir');
        $cast->umur = $request->input('umur');
        $cast->negara_asal = $request->input('negara_asal');
        $cast->description = $request->input('description');
        $cast->status = $request->input('status') == TRUE?'1':'0';

        $cast->save($validateData);
        return redirect('/dashboard/cast-controller')->with('success', 'Cast Berhasil Ditambahkan!');
    }

    // Route to Edit Cast Page
    public function edit($id){

        // Mendecrypt id
        $decryptedID = Crypt::decryptString($id);
        $cast = Cast::find($decryptedID);

        return view('admin.cast.edit', [
            'title' => 'Update Cast'
        ], compact('cast'));
    }

    // Update Cast Function
    public function update(Request $request, $id){
        $cast = Cast::find($id);

        // Validate Data
        $validateData = $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'tgl_lahir' => 'required',
            'tmpt_lahir' => 'required',
            'umur' => 'required',
            'negara_asal' => 'required',
            'description' => 'required'
        ]);

        // Update Photo Cast
        if($request->hasFile('img_cast')) 
        {
            // Check if photo is already exists or no
            $path = 'assets/uploads/cast/'.$cast->img_cast;
            if(File::exists($path)) {
                File::delete($path);
            }
            
            // Store the image
            $file = $request->file('img_cast');
            $ext = $file->getClientOriginalExtension();
            $filename = time().'.'.$ext;
            $file->move('assets/uploads/cast/',$filename);
            $cast->img_cast = $filename;

        }

        // Input data to database
        $cast->name = $request->input('name');
        $cast->slug = $request->input('slug');
        $cast->tgl_lahir = $request->input('tgl_lahir');
        $cast->tmpt_lahir = $request->input('tmpt_lahir');
        $cast->umur = $request->input('umur');
        $cast->negara_asal = $request->input('negara_asal');
        $cast->description = $request->input('description');
        $cast->status = $request->input('status') == TRUE?'1':'0';

        $cast->update($validateData);
        return redirect('/dashboard/cast-controller')->with('success', 'Cast Berhasil di Update!');
    }
    
    // Delete Cast Function
    public function delete($id) {

        // Mendecrypt id
        $decryptedID = Crypt::decryptString($id);
        $cast = Cast::find($decryptedID);

        if($cast->img_cast) {
            $path = 'assets/uploads/cast/'.$cast->img_cast;
            if(File::exists($path)) {
                File::delete($path);
            }
        }

        $cast->delete();
        return back()->with('success', 'Cast Berhasil di Hapus!');
    }
}
