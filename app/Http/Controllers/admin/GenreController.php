<?php

namespace App\Http\Controllers\admin;

use App\Models\Genre;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class GenreController extends Controller
{
    // Route To Genre Controller Page
    public function index() {
        $genre = Genre::latest()->get();

        return view('admin.genre.genre', [
            'title' => 'Genre Data'
        ], compact('genre'));
    }

    // Route To Genre Add Page
    public function add() {
        return view('admin.genre.add', [
            'title' => 'Tambah Genre'
        ]);
    }

    // Add Genre Data Function
    public function store(Request $request) {
        $genre = new Genre();

        // Validasi Data Genre
        $validateData = $request->validate([
            'name' => 'required|unique:genre',
            'slug' => 'required',
            'description' => 'required'
        ]);

        $genre->name = $request->input('name');
        $genre->slug = $request->input('slug');
        $genre->description = $request->input('description');
        $genre->status = $request->input('status') == TRUE?'1':'0';

        $genre->save($validateData);
        return redirect('/dashboard/genre-controller')->with('success', 'Genre Berhasil Ditambahkan!');
    }

    // Route To Genre Edit Page
    public function edit($id) {

        // Mendecrypt id
        $decryptedID = Crypt::decryptString($id);
        $genre = Genre::find($decryptedID);

        return view('admin.genre.edit', [
            'title' => 'Edit Genre'
        ], compact('genre'));
    }

    // Update Genre Data Function
    public function update(Request $request, $id) {
        $genre = Genre::find($id);

        // Validasi Data Genre
        $validateData = $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required'
        ]);

        $genre->name = $request->input('name');
        $genre->slug = $request->input('slug');
        $genre->description = $request->input('description');
        $genre->status = $request->input('status') == TRUE?'1':'0';

        $genre->update($validateData);
        return redirect('/dashboard/genre-controller')->with('success', 'Genre Berhasil Ditambahkan!');
    }

    // Delete Genre Data Function
    public function delete($id) {

        // Mendecrypt id
        $decryptedID = Crypt::decryptString($id);
        $genre = Genre::find($decryptedID);

        $genre->delete();
        return back()->with('success', 'Genre Berhasil di Hapus!');
    }
}
