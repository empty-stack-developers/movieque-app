<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    // Route to Movie Table Page
    public function index() {
        return view('admin.movie.movie', [
            'title' => 'Movie Data'
        ]);
    }

    public function add() {
        return view('admin.movie.add', [
            'title' => 'Tambah Movie'
        ]);
    }
}
