<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    // Route To Login Page
    public function index() {
        return view('user.login.login', [
            'title' => 'Login'
        ]);
    }

    // Fungsi Login
    public function authenticate(Request $request) {

        // Validasi Data
        $validateData = $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required'
        ]);

        if (Auth::attempt($validateData)) {
            $request->session()->regenerate();

            if (Auth::user()->role_as == '1' || Auth::user()->role_as == '2' ) {
                return redirect('/dashboard')->with('status', 'Welcome To Dashboard!');
            } elseif (Auth::user()->role_as == '0') {
                return redirect('/')->with('status', 'Success Login!');
            }

        }

        return back()->with('failed', 'Login Failed!');
    }

    // Fungsi Logout
    public function logout(Request $request) {

        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/')->with('success', 'Logout Success!');
    }
}
