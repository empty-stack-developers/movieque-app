<?php

namespace App\Http\Controllers\user;

use App\Models\Genre;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AllMoviesController extends Controller
{
    public function index() {
        $genre = Genre::latest()->where('status', '1')->get();

        return view('user.allmovies.allmovies', [
            'title' => 'All Movies'
        ], compact('genre'));
    }
}
