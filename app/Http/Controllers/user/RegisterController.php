<?php

namespace App\Http\Controllers\user;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    // Route To Register Page
    public function index() {
        return view('user.register.register', [
            'title' => 'Register'
        ]);
    }

    // Fungsi Register
    public function register(Request $request) {
        
        // Validasi Data 
        $validateData = $request->validate([
            'name' => 'required|max:100',
            'username' => 'required|max:100',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:8|same:re_password'
        ]);

        // Bcrypt Password
        $validateData['password'] = bcrypt($validateData['password']);

        User::create($validateData);

        return redirect('/login')->with('success', 'Register Success, Please Login!');
    }
}
